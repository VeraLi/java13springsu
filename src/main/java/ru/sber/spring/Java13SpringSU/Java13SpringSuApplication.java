package ru.sber.spring.Java13SpringSU;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.sper.spring.Java13SpringSU.dbexample.dao.BookDAO;
import ru.sper.spring.Java13SpringSU.dbexample.dao.UserDAO;
import ru.sper.spring.Java13SpringSU.dbexample.model.User;

import java.time.LocalDate;

@SpringBootApplication
public class Java13SpringSuApplication implements CommandLineRunner {
	private final BookDAO bookDAO;
	private final UserDAO userDAO;

	public Java13SpringSuApplication(BookDAO bookDAO, UserDAO userDAO) {
		this.bookDAO = bookDAO;
		this.userDAO = userDAO;
	}
	public static void main(String[] args) {
		SpringApplication.run(Java13SpringSuApplication.class, args);
	}

    }@Override
    public void run(String... args) throws Exception {
//			userDAO.addUser(new User(1, "Иванов", "Иван",
//					LocalDate.of(2000, 12, 11),
//					"+7-901-234-56-99", "farfr312a@mail.ru",
//					"Недоросль2, Доктор Живаго2"));
//			userDAO.addUser(new User(2, "Петр", "Петров",
//					LocalDate.of(1998, 1, 15),
//					"+7-922-00-99-99", "golang2@gmail.com",
//					"Сестра моя - жизнь, Путешествие из Петербурга в Москву"));
	        userDAO.getUserBooks("+7-901-234-56-99");
            }
}
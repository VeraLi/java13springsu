package ru.sber.spring.Java13SpringSU.dbexample.constans;

public interface DBConstans {
    String DB_HOST = "localhost";
    String DB = "local_db";
    String USER = "postgres";
    String PASSWORD = "12345";
    String PORT = "5432";
}
package ru.sber.spring.Java13SpringSU.dbexample.model;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Book {
    private Integer bookId;
    private String bookTitle;
    private String bookAuthor;
    private Date dateAdded;
}

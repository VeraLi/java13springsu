package ru.sber.spring.Java13SpringSU.dbexample.model;

import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {
    private Integer userId;
    private String userSurname;
    private String userName;
    private LocalDate userBirthday;
    private String userPhoneNumber;
    private String userEmail;
    private String userTitlesOfBorrowedBooks;
}
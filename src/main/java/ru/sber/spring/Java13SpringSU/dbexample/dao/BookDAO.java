package ru.sber.spring.Java13SpringSU.dbexample.dao;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.sper.spring.Java13SpringSU.dbexample.model.Book;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
@Scope("prototype")
public class BookDAO {
    private final Connection connection;

    public BookDAO(Connection connection) {
        this.connection = connection;
    }

    public void getInfoAboutBooksByTitles(String[] titlesOfBooks) throws SQLException {
        for (String e : titlesOfBooks) {
            PreparedStatement selectQuery = connection.prepareStatement("select * from books where title = ?");
            selectQuery.setString(1, e);
            ResultSet resultSet = selectQuery.executeQuery();
            Book book = new Book();
            while (resultSet.next()) {
                book.setBookId(resultSet.getInt("id"));
                book.setBookTitle(resultSet.getString("title"));
                book.setBookAuthor(resultSet.getString("author"));
                book.setDateAdded(resultSet.getDate("date_added"));
                System.out.println(book);
            }
        }
    }
}
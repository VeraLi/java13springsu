create table users
(
    id serial primary key,
    surname varchar(30) not null,
    namee varchar(30) not null,
    birthday date not null,
    phone_number varchar(30) unique,
    email varchar(30) unique,
    titles_of_borrowed_books varchar(500)
);

select * from users;
commit;
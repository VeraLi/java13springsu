package ru.sber.spring.Java13SpringSU.dbexample.DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static ru.sper.spring.Java13SpringSU.dbexample.constans.DBConstans.*;

public enum DBApp {
    INSTANCE;

    private Connection connection;

    public Connection newConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:postgresql://" + DB_HOST + ":" + PORT + "/" + DB,
                USER, PASSWORD);
    }
}
// Доп2
// Проверка, является ли введенное число простым или нет

import java.util.Random;
public class SimpleNumber {
    public static void main(String[] args) {
        int value, number;
        Random random = new Random();

        value = random.nextInt(10);
        number = random.nextInt(10);
        if(value > 0 && number > 0 && value % number == 0){
            System.out.print("Число простое!");
        }else{
            System.out.print("Число не простое!");

        }

    }
}

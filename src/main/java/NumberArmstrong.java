// Доп1
// Проверка, является ли введение число - числом Армстронга

public class NumberArmstrong {
    public static void main(String[] args) {
        System.out.println(searchNextNumberArmstrong(10));
    }

    public static int searchNextNumberArmstrong(int x) {
        int result = x + 1;
        while (!numberIsArmstrong(result)) {
            result++;
        }
        return result;
    }

    public static boolean numberIsArmstrong(int number) {
        int result = 0;
        int temp = number;
        int countDigits = (int) Math.log10(number) + 1;
        while (temp != 0) {
            result += Math.pow(temp % 10, countDigits);
            temp /= 10;
        }
        return number == result;
    }
}
